import json
from DataIngestion import balance_sheet_data_location, cash_flow_data_location, income_data_location
from DataInvestigator import DataExtraction

class_balance_sheet = 'BALANCE_SHEET'
class_cash_flow = 'CASH_FLOW_STATEMENT'
class_income_statement = 'INCOME_STATEMENT'

prob_any_class = 1/3

def main(test_file):
    income_knowledge_dict = loadClassKnowledge(income_data_location)
    cash_flow_knowledge = loadClassKnowledge(cash_flow_data_location)
    balance_knowledge = loadClassKnowledge(balance_sheet_data_location)

    total_trait_count = sum(income_knowledge_dict.values())
    total_trait_count += sum(cash_flow_knowledge.values())
    total_trait_count += sum(balance_knowledge.values())

    modal_words = DataExtraction().identifyDistinctWordsInGroupsAndFrequency(test_file)

    prob_income = 0
    prob_cash = 0
    prob_balance = 0

    for trait in modal_words:
        if trait in income_knowledge_dict:
            prob_trait_in_class = income_knowledge_dict[trait] / total_trait_count
            prob_trait_occurring = (income_knowledge_dict[trait] + cash_flow_knowledge[trait] + balance_knowledge[trait]) / total_trait_count
            prob_income += applyBayesTheorem(prob_trait_in_class, prob_any_class, prob_trait_occurring)

        if trait in cash_flow_knowledge:
            prob_trait_in_class = cash_flow_knowledge[trait] / total_trait_count
            prob_trait_occurring = (income_knowledge_dict[trait] + cash_flow_knowledge[trait] + balance_knowledge[trait]) / total_trait_count
            prob_cash += applyBayesTheorem(prob_trait_in_class, prob_any_class, prob_trait_occurring)

        if trait in balance_knowledge:
            prob_trait_in_class = balance_knowledge[trait] / total_trait_count
            prob_trait_occurring = (income_knowledge_dict[trait] + cash_flow_knowledge[trait] + balance_knowledge[trait]) / total_trait_count
            prob_balance += applyBayesTheorem(prob_trait_in_class, prob_any_class, prob_trait_occurring)

    all_probabilities = [prob_income, prob_cash, prob_balance]

    classifications = []

    if prob_income == max(all_probabilities):
        classifications.append(class_income_statement)

    if prob_cash == max(all_probabilities):
        classifications.append(class_cash_flow)

    if prob_balance == max(all_probabilities):
        classifications.append(class_balance_sheet)

    if len(classifications) == 1:
        print("Classified as: " + classifications[0])
    else:
        print("Unable to ascertain specific classification...")
        if (len(classifications) > 0):
            print("Perhaps one of the following: " + str(classifications))


def loadClassKnowledge(knowledge_location):
    with open(knowledge_location) as source:
        return json.load(source)


def applyBayesTheorem(probTraitGivenClass, probClass, probTrait):
    return (probTraitGivenClass * probClass) / probTrait

if __name__=='__main__':
    main('../../sample_data/income_statement/microsoft.csv')





