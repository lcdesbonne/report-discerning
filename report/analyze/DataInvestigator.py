"""
Investigate data theories to spot any potential leads on the identification of differences
between the files
"""
import csv
import re
import pandas as pd
import os


class DataExtraction(object):

    def __init__(self, cash_flow_dir='../../sample_data/cash_flow_statement', balance_sheet_dir='../../sample_data/balance_sheet',income_statement_dir='../../sample_data/income_statement'):
        self.cash_flow_directory = cash_flow_dir
        self.balance_sheet_directory = balance_sheet_dir
        self.income_statement_directory = income_statement_dir

    def find_eligible_words_for_classification(self):
        cash_flow_top_words = self.get_top_words_for_class(self.cash_flow_directory)
        balance_sheet_top_words = self.get_top_words_for_class(self.balance_sheet_directory)
        income_statement_top_words = self.get_top_words_for_class(self.income_statement_directory)

        all_top_words = cash_flow_top_words
        all_top_words = all_top_words.union(balance_sheet_top_words)
        all_top_words = all_top_words.union(income_statement_top_words)

        print("Top words across all classes...")
        print(all_top_words)

    # Unsure whether the values from here are useful...
    def investigate_value_params(self):
        print("Cash flow values....")
        self.investigateValueContent(self.cash_flow_directory)
        print()
        print("Balance sheet values...")
        self.investigateValueContent(self.balance_sheet_directory)
        print()
        print("Income statement values...")
        self.investigateValueContent(self.income_statement_directory)

    def get_top_words_for_class(self, directory):
        common_words_for_all = set()

        for file_name in os.listdir(directory):
            top_words_set = self.identifyDistinctWordsInGroupsAndFrequency(directory + "/" + file_name)

            if not common_words_for_all:
                common_words_for_all = top_words_set
            else:
                common_words_for_all = common_words_for_all.intersection_update(top_words_set)

        return common_words_for_all

    def identifyDistinctWordsInGroupsAndFrequency(self, file_path):
        noNumbersPattern = re.compile(".*[^0-9]+.*")
        # Analyzing balance sheet
        with open(file_path) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            content_word_dict = dict()
            for row in  csv_reader:
                if line_count == 0:
                    line_count += 1
                    continue
                else:
                    if len(content_word_dict) == 0:
                        content_word_dict = dict([(word.upper(), 1) for word in row[1].split(' ') if noNumbersPattern.match(word)])
                    else:
                        for word in row[1].split(' '):
                            if noNumbersPattern.match(word):
                                if word.upper() in content_word_dict:
                                    content_word_dict[word.upper()] += 1
                                else:
                                    content_word_dict.update({word.upper(): 1})

            print("Top 8 modal words in file: " + file_path)

            sorted_words_by_frequency_list = {key: val for key, val in sorted(content_word_dict.items(), key=lambda item: item[1], reverse=True)}
            numberDisplayed = 1
            top_words = set()
            for wordKey in sorted_words_by_frequency_list.keys():
                if wordKey != 'AND' and wordKey != 'OF' and wordKey != 'IN':
                    print(wordKey + ":" + str(sorted_words_by_frequency_list[wordKey]))
                    top_words.add(wordKey)
                    numberDisplayed += 1

                if numberDisplayed > 8:
                    break;

            return top_words;

    def investigateValueContent(self, file_path):
        for file_name in os.listdir(file_path):
            file_dataFrame = pd.read_csv(file_path + '/' + file_name)
            value_data = file_dataFrame['Value']
            value_data = value_data[value_data.notnull()]
            value_data = value_data.apply(str)
            value_data = value_data.apply(lambda val : re.sub('[$()]','', val))

            numeric_list = pd.to_numeric(value_data, downcast='float')
            numeric_list.sort_values()

            minVal = min(numeric_list)
            maxVal = max(numeric_list)

            print("Numbers from file: " + file_path + "/" + file_name)
            print("Min value: " + str(minVal))
            print("Max value: " + str(maxVal))

            print("Range: " + str(maxVal - minVal))

            if minVal == 0:
                print("Order magnitude: " + str(maxVal))
            else:
                print("Order of magnitude: " + str(maxVal / minVal))


if __name__ == '__main__':
    d_extraction = DataExtraction()
    d_extraction.find_eligible_words_for_classification()
    d_extraction.investigate_value_params()
