"""
    Ingest data to a json file representing the top modal words across all classes
"""

from DataInvestigator import DataExtraction
import os
import json

# word list identified as a result of DataInvestigator.find_eligible_words_for_classification()
word_list = ['AUTOMOTIVE', 'ISSUED', 'INCOME', 'PROCEEDS', 'ACTIVITIES', 'TOTAL', 'FROM', 'LIABILITIES', 'CASH', 'TO', 'COMMON', 'REVENUES', 'PORTION', 'CURRENT', 'LOSS', 'NET', 'OPERATING', 'ASSETS', 'OTHER']
balance_sheet_data_location = "../repository/balance_sheet.txt"
cash_flow_data_location = "../repository/cash_flow_sheet.txt"
income_data_location = "../repository/income_sheet.txt"

def ingest_balance_sheet_data(source_dir, data_destination):
    balance_sheet_word_dict = dict([(word, 0) for word in word_list])

    # identifyDistinctWordsInGroupsAndFrequency
    data_extractor = DataExtraction()

    for file_name in os.listdir(data_extractor.balance_sheet_directory):
        top_words = data_extractor.identifyDistinctWordsInGroupsAndFrequency(source_dir + "/" + file_name)

        for word in top_words:
            if word in balance_sheet_word_dict:
                balance_sheet_word_dict[word] += 1

    print(balance_sheet_word_dict)

    with open(data_destination, 'w') as dest_file:
        json.dump(balance_sheet_word_dict, dest_file)



if __name__ == '__main__':
    ingest_balance_sheet_data(DataExtraction().cash_flow_directory, cash_flow_data_location)
    ingest_balance_sheet_data(DataExtraction().income_statement_directory, income_data_location)

