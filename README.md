## Report Discerning


**General Info:** 
Programme has been written using python 3.6. 
- DataInvestigator.py explores potential traits that could be useful for classification
- DataIngestion.py creates trait information about the classes and stores them in json files
- ReportClassifier.py is the classification programme. The main function takes the path to the report to classify

